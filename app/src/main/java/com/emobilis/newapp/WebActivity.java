package com.emobilis.newapp;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;

public class WebActivity extends AppCompatActivity {
    //declare that element
    WebView webView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_web);

        //ref
        webView = findViewById(R.id.web);
        //attributes
        //create the new instance of the webview class
        webView.setWebViewClient(new WebViewClient());
        //load url
        webView.loadUrl("https://material.io/");
        //setting on web
        WebSettings webSettings = webView.getSettings();
        //enable javascript
        webSettings.setJavaScriptEnabled(true);
    }

    //override the onBackPressed

    @Override
    public void onBackPressed() {
        if (webView.canGoBack()){
            webView.goBack();
        } else {
            super.onBackPressed();
        }
    }
}
