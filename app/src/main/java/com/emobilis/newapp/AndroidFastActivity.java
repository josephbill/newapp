package com.emobilis.newapp;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;


import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONArrayRequestListener;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.androidnetworking.interfaces.UploadProgressListener;
import com.esafirm.imagepicker.features.ImagePicker;
import com.esafirm.imagepicker.model.Image;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.util.List;

import cn.pedant.SweetAlert.SweetAlertDialog;

public class AndroidFastActivity extends AppCompatActivity {
    private static final int PICK_FROM_GALLERY =2222;
    EditText submission,ed1,ed2;
    Button btnUpload,btnGet,btnPost;
    File file1,file2,file3,file4;
    String user_input;
    TextView textView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_android_fast);

        btnUpload = findViewById(R.id.uploadFile);
        submission = findViewById(R.id.etSub);
        ed1 = findViewById(R.id.etSub1);
        ed2 = findViewById(R.id.etSub2);
        btnGet = findViewById(R.id.uploadFile2);
        btnPost = findViewById(R.id.uploadFile3);
        textView = findViewById(R.id.text);

        btnGet.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getMethod();
            }
        });

        btnPost.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                postMethod();
            }
        });

        btnUpload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                uploadFiles(file1, file2, file3, file4, user_input);
            }
        });

        //intialize Andtoid fast networking
        AndroidNetworking.initialize(getApplicationContext());

    }
    //get
    private void getMethod() {
        //pick users input
        String filter = ed1.getText().toString().trim();

        //progress
        //shown user progress
        final SweetAlertDialog loadingDialog = new SweetAlertDialog(AndroidFastActivity.this,SweetAlertDialog.PROGRESS_TYPE);
        loadingDialog.setTitleText("Fetching data  ...");//Processing your request
        loadingDialog.setCancelable(true);
        loadingDialog.setCanceledOnTouchOutside(false);
        loadingDialog.show();
        //url
        String URL = "https://pixabay.com/api/?key=5303976-fd6581ad4ac165d1b75cc15b3&q=kitten&image_type=photo&pretty=true";
        AndroidNetworking.get(URL)
                //addPathParameter
        .addPathParameter("id", String.valueOf(Integer.parseInt(filter)))
        .setPriority(Priority.HIGH)
                .build()
               .getAsJSONObject(new JSONObjectRequestListener() {
                   @Override
                   public void onResponse(JSONObject response) {
                       loadingDialog.dismiss();
                       try {
                           JSONArray jsonArray = response.getJSONArray("hits");
                           for(int i = 0; i < jsonArray.length(); i++){
                               JSONObject jsonObject = jsonArray.getJSONObject(i);
                               String urlpage = jsonObject.getString("pageURL");

                               textView.setText(urlpage);
                           }
                       } catch (JSONException e) {
                           e.printStackTrace();
                       }
                   }

                   @Override
                   public void onError(ANError anError) {
                       Toast.makeText(AndroidFastActivity.this, anError.getErrorBody(), Toast.LENGTH_SHORT).show();
                   }
               });


        //without filter
        AndroidNetworking.get(URL)
                .setPriority(Priority.HIGH)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        loadingDialog.dismiss();
                        try {
                            JSONArray jsonArray = response.getJSONArray("hits");
                            for (int i = 0; i < jsonArray.length(); i++){
                                JSONObject jsonObject = jsonArray.getJSONObject(i);

                                String pageURL = jsonObject.getString("pageURL");
                                int likes = jsonObject.getInt("likes");

                                String like = String.valueOf(likes);

                                textView.append(pageURL + ", " + like);

                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onError(ANError anError) {
                        Toast.makeText(AndroidFastActivity.this, anError.getErrorBody(), Toast.LENGTH_SHORT).show();
                    }
                });

    }

    //post
    private void postMethod(){
           //pick user input
        String name = ed1.getText().toString().trim();
        String  job  = ed2.getText().toString().trim();
        //ref url
        String URL = "https://reqres.in/api/users";

        //shown user progress
        final SweetAlertDialog loadingDialog = new SweetAlertDialog(AndroidFastActivity.this,SweetAlertDialog.PROGRESS_TYPE);
        loadingDialog.setTitleText("Posting data  ...");//Processing your request
        loadingDialog.setCancelable(true);
        loadingDialog.setCanceledOnTouchOutside(false);
        loadingDialog.show();

        AndroidNetworking.post(URL)
                .addBodyParameter("name",name)
                .addBodyParameter("job",job)
                .setPriority(Priority.HIGH)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        loadingDialog.dismiss();
                        Toast.makeText(AndroidFastActivity.this, response.toString(), Toast.LENGTH_LONG).show();
                    }

                    @Override
                    public void onError(ANError anError) {
                        Toast.makeText(AndroidFastActivity.this, anError.getErrorBody(), Toast.LENGTH_LONG).show();

                    }
                });
    }


    public void multipleImages(View v){
        openGallery();
    }
    //method to allow us to pick multiple files
    private void openGallery() {
        //first check if permissions are allowed
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED
                || ActivityCompat.checkSelfPermission(this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.CAMERA}, PICK_FROM_GALLERY);
        } else {

            ImagePicker.create(this)
                    .multi() // multi mode (default mode)
                    .limit(4)// max images can be selected (99 by default)
                    .start();
        }

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        if (ImagePicker.shouldHandle(requestCode, resultCode, data)) {
            // Get a list of picked images
            final List<Image> images = ImagePicker.getImages(data);
            //message to user upone selecting the image s
            SweetAlertDialog selectedDialog = new SweetAlertDialog(this, SweetAlertDialog.SUCCESS_TYPE);
            selectedDialog.getProgressHelper().setBarColor(Color.parseColor("#A5DC86"));
            selectedDialog.setTitleText(images.size() + " Images Selected");
            selectedDialog.setCancelable(true);
            selectedDialog.setCanceledOnTouchOutside(false);
            selectedDialog.setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                @Override
                public void onClick(SweetAlertDialog sweetAlertDialog) {
                    sweetAlertDialog.dismissWithAnimation();  //dismiss the alert
                    //check the images selected
                    if (images.size() < 4) {
                        SweetAlertDialog errorDialog = new SweetAlertDialog(AndroidFastActivity.this, SweetAlertDialog.ERROR_TYPE);
                        errorDialog.setTitleText("Please select at least 4 images");
                        errorDialog.setCancelable(false);
                        errorDialog.show();
                    } else {
                        //save references to the images and also call the method to upload my data using Android FAST
                        //pick the user input
                         user_input = submission.getText().toString().trim();
                         file1 = new File(images.get(0).getPath());
                         file2 = new File(images.get(1).getPath());
                         file3 = new File(images.get(2).getPath());
                         file4 = new File(images.get(3).getPath());
                        uploadFiles(file1, file2, file3, file4, user_input);
                    }

                }
            });
            selectedDialog.show();

        }
        super.onActivityResult(requestCode, resultCode, data);

    }



    private void uploadFiles(File file1, File file2, File file3, File file4, String user_input) {
        Log.d("bill", "Files are : " + file1 + file2 + file3 + file4 + "User_input is " + user_input);
        //method we use the upload request media
        //first the URL we want to submit to
        String URL = "";

        //shown user progress
        final SweetAlertDialog loadingDialog = new SweetAlertDialog(AndroidFastActivity.this,SweetAlertDialog.PROGRESS_TYPE);
        loadingDialog.setTitleText("Uploading message ...");//Processing your request
        loadingDialog.setCancelable(true);
        loadingDialog.setCanceledOnTouchOutside(false);
        loadingDialog.show();

        //our call to android networking
        AndroidNetworking.upload(URL)
                //add the files , if the file is an image or an attached document then i use the method .addMultiPartFile then
                //if the submission is of a text nature using the upload request , use the method .addMultiPartParameter
                .addMultipartFile("name",file1)
                .addMultipartFile("name2",file2)
                .addMultipartFile("name3",file3)
                .addMultipartFile("name4",file4)
                .addMultipartParameter("name5",user_input)
                //next select the level
                .setPriority(Priority.HIGH)
                //build the request
                .build()
                //set interface method for the multipartFile response
                .setUploadProgressListener(new UploadProgressListener() {
                    @Override
                    public void onProgress(long bytesUploaded, long totalBytes) {
                         //toast
                        Toast.makeText(AndroidFastActivity.this, "Image Uploaded", Toast.LENGTH_SHORT).show();
                    }
                })
                //set up the interface method for the multipartparameter response
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        //dismiss the dialog
                        loadingDialog.dismiss();
                        Toast.makeText(AndroidFastActivity.this, "Connection successful", Toast.LENGTH_SHORT).show();
                    }

                    @Override
                    public void onError(ANError anError) {
                        //dismiss the dialog
                        loadingDialog.dismiss();
                        Toast.makeText(AndroidFastActivity.this, "Error occurred check your connection", Toast.LENGTH_SHORT).show();
                    }
                });


    }



}
