package com.emobilis.newapp;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void gotoWeb(View v){
        Intent intent = new Intent(MainActivity.this,WebActivity.class);
        startActivity(intent);
    }
    public void fastActivity(View v){
        Intent intent = new Intent(MainActivity.this,AndroidFastActivity.class);
        startActivity(intent);
    }

    public void gson(View v){
        Intent intent = new Intent(MainActivity.this,GSONActivity.class);
        startActivity(intent);
    }

    public void gsonSP(View v){
        Intent intent = new Intent(MainActivity.this,ArrayListGSON.class);
        startActivity(intent);
    }
}
