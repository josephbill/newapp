package com.emobilis.newapp;

public class RecylerModel {
    //variables
    private String mline1;
    private String mline2;

    //constructor
    public RecylerModel(String mline1,String mline2){
        this.mline1 = mline1;
        this.mline2 = mline2;
    }

    public String getMline1() {
        return mline1;
    }

    public void setMline1(String mline1) {
        this.mline1 = mline1;
    }

    public String getMline2() {
        return mline2;
    }

    public void setMline2(String mline2) {
        this.mline2 = mline2;
    }
}
