package com.emobilis.newapp;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;

public class ArrayListGSON extends AppCompatActivity {
    ArrayList<RecylerModel> recylerModelArrayList;
    private RecyclerView mRecyclerView;
    private RecyclerAdapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_array_list_g_s_o_n);
        //should pick user input
        loadData();
        buildRecyclerView();
        setInsertButton();
        Button buttonSave = findViewById(R.id.button_save);
        buttonSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                saveData();
            }
        });
    }

    private void loadData() {
        SharedPreferences sharedPreferences = getSharedPreferences("shared preferences", MODE_PRIVATE);
        //intialize gson
        Gson gson = new Gson();
        //json
        String json = sharedPreferences.getString("task list", null);
        //type converter
        Type type = new TypeToken<ArrayList<RecylerModel>>() {}.getType();
        //load the JSON in SP to arraylist item
        recylerModelArrayList = gson.fromJson(json,type);
        //check if SP is null
        if (recylerModelArrayList == null){
            //if null create a new instance of the ArrayList
            recylerModelArrayList = new ArrayList<>();
        }
    }

    private void buildRecyclerView() {
        //ref
        mRecyclerView = findViewById(R.id.recyclerview);
        //size
        mRecyclerView.setHasFixedSize(true);
        //new instance LINEARLAYOUT MANAGER
        mLayoutManager = new LinearLayoutManager(this);
        //new instance for adapter
        mAdapter = new RecyclerAdapter(this,recylerModelArrayList);
        mRecyclerView.setLayoutManager(mLayoutManager);
        //set adapter to recyclerview
        mRecyclerView.setAdapter(mAdapter);
    }

    private void saveData() {
        SharedPreferences sharedPreferences = getSharedPreferences("shared preferences", MODE_PRIVATE);
        //create an editor instance
        SharedPreferences.Editor editor = sharedPreferences.edit();
        //create a GSON reference
        Gson gson = new Gson();
        String json = gson.toJson(recylerModelArrayList);
        //store that list in shared preferences
        editor.putString("task list", json);
        editor.apply();
    }

    private void setInsertButton() {
        Button btn  = findViewById(R.id.button_insert);
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                EditText line1 = findViewById(R.id.edittext_line_1);
                EditText line2 = findViewById(R.id.edittext_line_2);
                //insertitem , insert our inputs into our arrayList
                insertItem(line1.getText().toString(), line2.getText().toString());
            }
        });
    }

    private void insertItem(String line1, String line2) {
        //add inputs to model class
        recylerModelArrayList.add(new RecylerModel(line1,line2));
        //notify the adapter that data has been added
        mAdapter.notifyItemInserted(recylerModelArrayList.size());

    }
}
