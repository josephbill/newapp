package com.emobilis.newapp;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

public class RecyclerAdapter extends RecyclerView.Adapter<RecyclerAdapter.RecyclerViewHolder> {
    private Context context;
    private ArrayList<RecylerModel> recylerModelArrayList;

    //constructor
    public RecyclerAdapter(Context context,ArrayList<RecylerModel> recylerModels){
        this.context = context;
        this.recylerModelArrayList = recylerModels;
    }

    public static class RecyclerViewHolder extends RecyclerView.ViewHolder {
        TextView line1,line2;

        public RecyclerViewHolder(@NonNull View itemView) {
            super(itemView);

            line1 = itemView.findViewById(R.id.textview_line1);
            line2 = itemView.findViewById(R.id.textview_line_2);
        }


    }
    @NonNull
    @Override
    public RecyclerAdapter.RecyclerViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.recycled_item, parent, false);
        RecyclerViewHolder evh = new RecyclerViewHolder(v);
        return evh;
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerAdapter.RecyclerViewHolder holder, int position) {
                 RecylerModel recylerModel = recylerModelArrayList.get(position);

                 holder.line1.setText(recylerModel.getMline1());
                 holder.line2.setText(recylerModel.getMline2());

    }

    @Override
    public int getItemCount() {
        return recylerModelArrayList.size();
    }
}
