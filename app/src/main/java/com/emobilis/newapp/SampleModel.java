package com.emobilis.newapp;

import com.google.gson.annotations.SerializedName;

public class SampleModel {
    @SerializedName("first_name")
    private String firstName;
    @SerializedName("age")
    private int age;
    @SerializedName("mail")
    private String mail;

    //constructor
    public SampleModel(String firstNames, int ages, String mails){
        firstName = firstNames;
        age = ages;
        mail = mails;
    }
}
