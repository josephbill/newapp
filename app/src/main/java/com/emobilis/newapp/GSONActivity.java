package com.emobilis.newapp;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;

import com.google.gson.Gson;

public class GSONActivity extends AppCompatActivity {
    TextView textView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_g_s_o_n);

        textView = findViewById(R.id.textJSON);

        //intialize
        Gson gson = new Gson();

        //serilization i.e. changing form JAVA OBJECT to JSON
        SampleModel sampleModel = new SampleModel("John",40,"john@john.com");
        String json = gson.toJson(sampleModel);

        Log.d("GSONActivity" , "JSON is " + json  );
        textView.setText(json);

        //deserialization i.e changing from JSON to JAVA OBJECT
        String json1 = "{\"first_name\":\"John\",\"age\":30,\"mail\":\"john@gmail.com\"}";
        //use GSON to deserialize
        SampleModel model  = gson.fromJson(json1,SampleModel.class);


    }
}
